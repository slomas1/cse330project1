//Don't

#ifndef threads_h
#define threads_h
#define STACK_SIZE 8192
#include "q.h"

void start_thread(void (*function)(void))
{
    // Allocate a stack
     void *stack = malloc(STACK_SIZE);

    // Allocate a TCB
     TCB_t *new_tcb = NewItem(); 

     // Call init_TCB with appropriate arguments
     init_TCB(new_tcb, function, stack, STACK_SIZE);

    // Call addQ to add this TCB into the “RunQ” which is a global header pointer
     AddQueue(RunQ, new_tcb);
}

void run()
{
    // Get a place to store the main context, for faking
    ucontext_t parent;

    // Magic Sauce
    getcontext(&parent);

    // Start first thread
    swapcontext(&parent, &(RunQ->head->context));
}

void yield() // similar to run
{
	ucontext_t parent;

    // Rotate the run Q
	RotateQueue(RunQ);
	getcontext(&parent);
     
     //swap the context, from previous thread to the thread pointed to by RunQ
	swapcontext(&parent,&(RunQ->head->context));}

#endif /* threads_h */
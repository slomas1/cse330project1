/*
/ CSE 330
/ Authors: Sean Williams, Sergio Lomas
/ Programing Project 1
/ Session: TTh 4:30
/ Professor: Dasgupta
*/
#include <stdio.h>
#include <stdlib.h>
#include "tcb.h"

//Queue struct containing the headpointer
typedef struct queue
{
	struct TBC_t *headptr;
};

struct TCB_t* newItem();
struct queue* newQueue();
struct TCB_t* DelQueue(struct queue* head);
void addQueue(struct queue* head, struct TCB_t *item);
void RotateQ(struct queue* head);

//Function to create a new queue
struct queue* newQueue()
{
	struct queue *newQueue = malloc(sizeof(struct queue));
	newQueue->headptr = NULL;

	return newQueue;
}

//Function to create a new queue element
struct TCB_t* newItem()
{
	struct TCB_t * item = malloc(sizeof(struct TCB_t));;
	item->next = NULL;
	item->prev = NULL;
	item->thread_id = NULL;

	return item;
}

//Function to add an item to the end of a queue
void addQueue(struct queue* head, struct TCB_t *item)
{
	struct TCB_t* seekQ = head->headptr;

	if (seekQ == NULL)
	{
		head->headptr = item;
		item->next = item;
		item->prev = item;
	}
	else
	{
		while (seekQ->next != head->headptr)
		{
			seekQ = seekQ->next;
		}

		item->next = head->headptr;
		head->headptr->prev = item;
		seekQ->next = item;
		item->prev = seekQ;
	}
}

//Function to delete the first item in a queue and return it
struct TCB_t* DelQueue(struct queue* head)
{
	struct TCB_t* item = head->headptr;

	head->headptr->next->prev = head->headptr->prev;
	head->headptr->prev->next = head->headptr->next;

	if (head->headptr == head->headptr->next) {
		head->headptr = NULL;
	}
	else {
		head->headptr = head->headptr->next;
	}
	return item;
}

//Function to rotate the queue
void RotateQ(struct queue* head)
{
	addQueue(head, DelQueue(head));
}
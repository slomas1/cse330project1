/*
/ CSE 330
/ Authors: Sean Williams, Sergio Lomas
/ Programing Project 2
/ Session: TTh 4:30
/ Professor: Dasgupta
*/
#include <stdio.h>
#include <stdlib.h>
#include "q.h"

struct queue* RunQ;

void run();
void start_thread(void(*function)(void));

void start_thread(void(*function)(void))
{
	void *stack = malloc(8192);
	struct TCB_t *newTCBt = newItem();
	init_TCB(newTCBt, function, stack, 8192);
	addQueue(RunQ, newTCBt);
}

void run()
{
	queue* Curr_Thread = RunQ;
	ucontext_t parent;
	getcontext(&parent);
	swapcontext(&parent, &(Curr_Thread->context));
}

void yield()
{
	queue* Prev_Thread = RunQ;
	RotateQ(RunQ);
	queue* Curr_Thread = RunQ;
	swapcontext(&(Prev_Thread->context), &(Curr_Thread->context));
}